//
// Created by root on 17-3-11.
//

#include "spdlog/spdlog.h"

#ifndef HIKVISIONCTRL_UTILS_H
#define HIKVISIONCTRL_UTILS_H

class SpdLog
{
public:
    SpdLog();
    ~SpdLog();

    static void set_log_level(uint32_t level);

public:
    static std::shared_ptr<spdlog::logger> console;
};

#endif //HIKVISIONCTRL_UTILS_H

