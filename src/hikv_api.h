//
// Created by root on 17-3-10.
//

#ifndef HIKVISIONCTRL_HIKV_API_H
#define HIKVISIONCTRL_HIKV_API_H

#include <iostream>
#include "ctrl_cfg.h"

class HikvisionAPI
{
public:
    HikvisionAPI();
    ~HikvisionAPI();

    int hikv_capture_picture(uint32_t channel, std::string &dev_picture);

    CtrlCfg *pctrl_cfg;
private:

};

#endif //HIKVISIONCTRL_HIKV_API_H


