//
// Created by root on 17-3-11.
//

#ifndef HIKVISIONCTRL_CTRL_CFG_H
#define HIKVISIONCTRL_CTRL_CFG_H

#include <iostream>
#include <map>

class CtrlCfg {
public:
    typedef struct dev_config_st {
        char dev_serial[64];
        char dev_ip[32];
        uint32_t dev_port;
        char dev_username[64];
        char dev_password[64];
    } Dev_Config_ST;
    typedef std::map<std::string, Dev_Config_ST> Dev_Config_Map;
    typedef struct yaml_config_st {
        bool on_timer_delete_file;
        uint32_t file_cache_interval;

        uint32_t sdklog_level;
        char sdklog_dir[128];
        bool sdklog_autodel;

        uint32_t applog_level;

        Dev_Config_Map dev_map;

        char snapshot_picture_path[128];
        uint32_t snapshot_picture_quality;
        uint32_t snapshot_picture_size;
        char upload_mode[32];
        char upload_path[32];

        char mqtt_host[32];
        uint32_t mqtt_port;
        uint32_t mqtt_keepalive;

    } Yaml_Config_ST;

    typedef std::map<std::string, bool> General_Map;
    typedef std::map<std::string, bool> Devinfo_Map;
    typedef std::map<std::string, bool> Snapshot_Map;
    typedef std::map<std::string, bool> Mqtt_Map;
    General_Map _general_map;
    Devinfo_Map _devinfo_map;
    Snapshot_Map _snapshot_map;
    Mqtt_Map _mqtt_map;

public:
    CtrlCfg();
    ~CtrlCfg();

    void parse_config(std::string &config_file);

    void set_config_file(std::string &cfg_file);
    std::string get_config_file();
    void set_config_st(Yaml_Config_ST conf);
    Yaml_Config_ST get_config_st();

    Dev_Config_Map get_dev_config_map();

    std::string get_yaml_config_info();
    bool update_general_config(std::string sdklog_dir, uint32_t sdklog_level, bool sdklog_autodel,
                               uint32_t applog_level, bool on_timer_delete_file, uint32_t file_cache_interval);
    bool update_devinfo_config(std::string serial, std::string ip, uint32_t port, std::string username, std::string password);
    bool update_snapshot_config(std::string picture_path,
                                uint32_t picture_quality,
                                uint32_t picture_size,
                                std::string upload_mode,
                                std::string upload_path);
    bool update_mqtt_config(std::string host, uint32_t port, uint32_t keepalive);

private:
    std::string _config_file;
    Yaml_Config_ST _config_st;
    Dev_Config_Map _dvr_cfg_map;
};

#endif //HIKVISIONCTRL_CTRL_CFG_H
