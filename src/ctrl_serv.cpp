#include <iostream>
#include <chrono>
#include <thread>
#include "ctrl_spdlog.h"
#include "ctrl_app.h"

int main(int argc, char *argv[])
{
    SpdLog::console->info("HikvisionCtrl start ......");
    std::string config_file = "/opt/HikvisionCtrl/config/ctrlparam.yaml";

    char id[50];
    memset(id, 0, sizeof(id));
    snprintf(id, 50, "hikvision_ctrl_%d", getpid());

    /*CtrlCfg *pcfg = new CtrlCfg();
    pcfg->set_config_file(config_file);
    pcfg->parse_config(config_file);
    SpdLog::console->info("HikvisionCtrl parse config complete");

    uint32_t level_val = pcfg->get_config_st().applog_level;
    SpdLog::set_log_level(level_val);

    //mosqpp::lib_init();
    CtrlMqtt *pmqtt;
    pmqtt = new CtrlMqtt(id);
    pmqtt->pctrl_cfg = pcfg;
    pmqtt->phikv_api = new HikvisionAPI();
    pmqtt->phikv_api->pctrl_cfg = pcfg;
    pmqtt->pctrl_app = new CtrlApp();
    pmqtt->pctrl_app->pctrl_cfg = pcfg;
    pmqtt->save_topics();
    SpdLog::console->info("HikvisionCtrl start mqtt");
    pmqtt->start_mqtt_cycle();
    //mosqpp::lib_cleanup();*/

    CtrlCfg *pcfg = new CtrlCfg();
    pcfg->set_config_file(config_file);
    pcfg->parse_config(config_file);
    SpdLog::console->info("HikvisionCtrl parse config complete");

    uint32_t level_val = pcfg->get_config_st().applog_level;
    SpdLog::set_log_level(level_val);

    mosqpp::lib_init();
    CtrlMqtt pmqtt(id);
    pmqtt.pctrl_cfg = pcfg;
    pmqtt.phikv_api = new HikvisionAPI();
    pmqtt.phikv_api->pctrl_cfg = pcfg;
    pmqtt.pctrl_app = new CtrlApp();
    pmqtt.pctrl_app->pctrl_cfg = pcfg;
    pmqtt.save_topics();
    pmqtt.loop_forever();
    mosqpp::lib_cleanup();

    return 0;
}

