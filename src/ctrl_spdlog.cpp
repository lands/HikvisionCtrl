//
// Created by root on 17-3-11.
//

#include "ctrl_spdlog.h"

std::shared_ptr<spdlog::logger> SpdLog::console = spdlog::stdout_color_mt("console");

SpdLog::SpdLog() {
    spdlog::set_level(spdlog::level::debug);
}

SpdLog::~SpdLog() {

}

void SpdLog::set_log_level(uint32_t level) {
    spdlog::level::level_enum level_val;
    if (level == 0) {
        level_val = spdlog::level::trace;
    } else if (level == 1) {
        level_val = spdlog::level::debug;
    } else if (level == 2) {
        level_val = spdlog::level::info;
    } else if (level == 3) {
        level_val = spdlog::level::warn;
    } else if (level == 4) {
        level_val = spdlog::level::err;
    } else if (level == 5) {
        level_val = spdlog::level::critical;
    }  else if (level == 6) {
        level_val = spdlog::level::off;
    }
    spdlog::set_level(level_val);
}





